console.log('Display the numbers from 1 to 20. (1, 2, 3, …,19, 20)');

for (let counter = 1; counter <= 20; counter++){
    console.log(counter)
}

console.log('Display the even numbers from 1 to 20. (2, 4, 6, …, 18, 20)');

for (let counter = 1; counter <= 20; counter++){
    if (counter%2 === 0){
        console.log(counter)
    }
}

console.log('Display the odd numbers from 1 to 20. (1, 3, 5, …, 17, 19)');

for (let counter = 1; counter <= 20; counter++){
    if (counter%2 != 0){
        console.log(counter);
    }
}

console.log('Display the multiples of 5 up to 100. (5, 10, 15, …, 95, 100)');

for (let counter = 1; counter <= 100; counter++){
    if (counter%5 === 0){
        console.log(counter);
    }
}

console.log('Display the square numbers up to 100. (1, 4, 9, …, 81, 100)');

for (let counter = 1; counter <= 100; counter++){
    if(Math.pow(counter, 2) <= 100){
        console.log(Math.pow(counter, 2));
    }
}

console.log('Display the numbers counting backwards from 20 to 1. (20, 19, 18, …, 2, 1)')

for (let counter = 20; counter > 0; counter--){
    console.log(counter);
}

console.log('Display the even numbers counting backwards from 20. (20, 18, 16, …, 4, 2)')

for ( let counter = 20; counter > 0; counter--){
    if (counter%2 === 0){
        console.log(counter);
        }
}

console.log('Display the odd numbers from 20 to 1, counting backwards. (19, 17, 15, …, 3, 1)')

for (let counter = 20; counter > 0; counter--){
    if (counter%2 != 0){
        console.log(counter);
    }
}

console.log('Display the multiples of 5, counting down from 100. (100, 95, 90, …, 10, 5)')

for (let counter = 100; counter > 0; counter--){
    if (counter%5 === 0){
        console.log(counter);
    }
}

console.log('Display the square numbers, counting down from 100. (100, 81, 64, …, 4, 1)')

for (let counter = 100; counter > 0; counter--){
    if (Math.pow(counter, 2) <= 100){
        console.log(Math.pow(counter, 2));
    }
}






